#include<iostream>
#include<stdlib.h>

int nr=0;
using namespace std;
class router{
    int *toNext;
public:
    router(){
        toNext = new int [nr];
    }
    void setDist(int index, int dist){
        this->toNext[index] =  dist;
    }
    int getDist(int index){
        return toNext[index];
    }
};

class DVRTable{
    int index=0, dist=0, rout=65;
    router *routerArray;
public:
    DVRTable(int num){
        nr = num;
        routerArray = new router[nr];
    }
    void enterDist(){
        for(index = 0, rout = 65; index < nr; index++, rout++){
            cout<<"Enter distsnce table for router: "<<char(rout)<<endl;
            for(int i = 0, j = 65; i < nr; i++, j++){
                cout<<"Enter distance from "<<char(j)<<":";
                cin>>dist;
                routerArray[index].setDist(i,dist);
            }
        }
    cout<<nr<<endl;
    }
    void display(){
        for(int ind = 0, rout = 65; ind < nr; ind++, rout++)
            for(int i = 0, j = 65; i < nr; i++,j++)
                for(int k = 0, l = 65; k< nr; k++, l++)
                    cout<<"From Router:"<<char(rout)<<" to Router:"<<char(j)<<" via Router:"<<char(l)<<" = "<<(routerArray[ind].getDist(i) + routerArray[i].getDist(k) - 1)<<endl;
    }
};

int main(){
    int nor, dist;
    cout<<"Enter the number of routers:";
    cin>>nor;
    DVRTable dt(nor);
    dt.enterDist();
    dt.display();
    return 0;
}
