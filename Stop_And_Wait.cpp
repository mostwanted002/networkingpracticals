#include<iostream>
#include<stdlib.h>
#include<cstring>


using namespace std;

class simulator{
    const int cw = 2048;
    int isNoisy, isACKR, isFR, noOfFrames;
    char *frame, *channel;
    void sender(){
        cout<<"Enter the frame to send: ";
        cin>>this->frame;
        cout<<"SENDER: Sending frame... \n";
        strcpy(this->channel, this->frame);
    }
    void receiver(){
        while(!isFR){
            resend();
        }
        cout<<"RECEIVER: Received Frame :"<<channel<<"\nSending ACK \n";
    }
    void resend(){
        cout << "SENDER: Timer timed out resending frame (either frame is lost or ACK is lost) \n";
        this->isFR = rand() % 2;
        this->isACKR = rand() % 2;
    }
    void senderACK(){
        if(!isACKR){
            resend();
            receiver();
        }
        cout<<"SENDER: Received ACK. Proceeding. \n";
    }
public:
    simulator(){
        this->frame = new char [cw];
        this->channel = new char [cw];
    }
    void simulate(int frames, int noisy){
        while(frames>0){
            if(noisy){
                this->isACKR = rand()%2;
                this->isFR = rand()%2;
            }
            else{
                this->isACKR = 1;
                this->isFR = 1;
            }
            sender();
            receiver();
            senderACK();
            frames--;
        }
        cout<<"END OF TRANSMISSION \n";
    }
};

int main(){
    int nf, noise;
    cout<<"(1) Stop & Wait in NOISY Channel \n";
    cout<<"(2) Stop & Wait in NON-NOISY Channel \n";
    cout<<"Select the type of channel: ";
    cin>>noise;
    system("clear");
    cout<<"Enter the number of frames: ";
    cin>>nf;
    simulator sm;
    switch(noise){
        case 1:{
            sm.simulate(nf, 1);
        }
        break;
        case 2:{
            sm.simulate(nf, 0);
        }
        break;
    }
}
